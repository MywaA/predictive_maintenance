import scipy
import numpy as np
import matplotlib.pyplot as plt

# fig 3.2

#Paramètres
N=2000
std=0.1
slope=0.005
mean_left=0

x1=[mean_left+np.random.normal(0,std) for i in range(N//2)]
x2=[mean_left+i*slope+np.random.normal(0,std) for i in range(N//2)]

samples=np.concatenate((x1,x2),axis=0)

plt.figure(1)
plt.plot(samples)
plt.title('Slope break')
plt.xlabel('time')
plt.ylabel('signal')
#plt.savefig('images/breaks/break_slope.png')
plt.show()