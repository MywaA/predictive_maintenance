import numpy as np
import matplotlib . pyplot as plt

# fig 3.3

def break_intermittent(std,N=2000):
    #Paramètres échelon
    mean_low = 0
    mean_high = 3
    
    #Paramètres palier
    mean_length = 50
    std_length = 15
    
    #Génération
    break_point = N//2 #Point de rupture
    samples = np.random.normal(mean_low,std,size=break_point)
    t=break_point+1

    while t<N:
        #Échelon haut
        time_high = np.abs(np.floor(np.random.normal(mean_length , std_length)))
        t+= time_high
        window_high = np.random.normal(mean_high, std, size=int(time_high))
        
        #Échelon bas
        time_low = np.abs(np.floor(np.random.normal(mean_length , std_length)))
        t+= time_low
        window_low = np.random.normal(mean_low, std, size=int(time_low))

        #Ajout des échelons
        samples = np.concatenate ((samples, window_high, window_low) , axis = 0)
    return samples[0:N] #On tronque les paliers qui dépassent

samples = break_intermittent(0.1)
plt.figure()
plt.plot(samples)
plt.title('Intermittent break')
plt.xlabel('Time')
plt.ylabel('Signal')
#plt.savefig('images/breaks/break_intermittent.png')
plt.show( )