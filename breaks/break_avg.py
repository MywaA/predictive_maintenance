import scipy
import numpy as np
import matplotlib.pyplot as plt

# fig 3.1

points=1000
#Partie 1
mean1=0
std1=0.1
#Partie 2
mean2=3
std2=0.1

samples1=np.random.normal(mean1,std1,size=points)
samples2=np.random.normal(mean2,std2,size=points)
samples3=np.random.normal(mean1,std2,size=points)
samples=np.concatenate((samples1,samples2,samples3),axis=0)

plt.figure(1)
plt.plot(samples)
plt.title('Average break')
plt.xlabel('time')
plt.ylabel('signal')
#plt.savefig('images/breaks/break_avg.png')
plt.show()