import scipy
import numpy as np
import matplotlib.pyplot as plt

import signals

# fig 3.4 à 3.9

def mean_modelisation(samples,w=100,d=1,N=2000):
  t_means=[]
  means=[]
  for t in range(int(w/2),N-int(w/2),d):
    small_sample=samples[t-int(w/2):t+int(w/2)] #fenetre courante
    small_sample_mean=0
    n = len(small_sample)
    for i in range(n):
      small_sample_mean+=small_sample[i]
    means.append(small_sample_mean/n) #calcul de la moyenne flottante
    t_means.append(t)
  diff_means=[]
  for k in range(len(means)-(w//d + 1)):
    diff_means.append(np.abs(means[k+w//d+1]-means[k]))
    #calcul de la dérivée discrète
  t_means=t_means[(w//d)//2:len(means)-(w//d)//2-1] #On tronque les positions 
  return diff_means,t_means

s_and_w = [[0.1,100],[0.1,200],[0.1,300],[0.5,100],[1,100]]
show = True
save = False

for sw in s_and_w :
  std,w = sw[0],sw[1]

  plt.figure(figsize=(12,6))
  plt.suptitle('Mean modelisation with std noise = {0} and window length = {1}'.format(std,w))

  # Tracé des ruptures
  avg_samples = signals.break_avg(std, 3000)
  slope_samples = signals.break_slopes(std, 2000)
  intermittent_samples = signals.break_intermittent(std, N=2000)

  plt.subplot(3,2,1)
  plt.plot(avg_samples)
  plt.xlabel('time')
  plt.ylabel('signal')

  plt.subplot(3,2,3)
  plt.plot(slope_samples)
  plt.xlabel('time')
  plt.ylabel('signal')

  plt.subplot(3,2,5)
  plt.plot(intermittent_samples)
  plt.xlabel('time')
  plt.ylabel('signal')

  # Tracé des caractéristiques
  diff_means,t_means = mean_modelisation(avg_samples,w,d=1,N=3000)

  plt.subplot(3,2,2)
  plt.plot(t_means,diff_means)
  plt.xlabel('time')
  plt.ylabel('feature')

  diff_means,t_means = mean_modelisation(slope_samples,w,d=1,N=2000)

  plt.subplot(3,2,4)
  plt.plot(t_means,diff_means)
  plt.xlabel('time')
  plt.ylabel('feature')

  diff_means,t_means = mean_modelisation(intermittent_samples,w,d=1,N=2000)

  plt.subplot(3,2,6)
  plt.plot(t_means,diff_means)
  plt.xlabel('time')
  plt.ylabel('feature')

  std=str(std)
  std=std.replace('.', '')

  if save :
    plt.savefig('images/modelisations/modelisation_mean/modelisation_mean_std_'+str(std)+'_w_'+str(w)+'.png')
  
  if show :
    plt.show()

  print("s : ", std)
  print("w : ", w)