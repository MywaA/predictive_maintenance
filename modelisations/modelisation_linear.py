import numpy as np
import matplotlib.pyplot as plt

import signals

# fig 3.10 à 3.17

def linear(samples, w=100, d=1):
    N = len(samples)
    slopes = []
    k = 0  # slope
    for i in range(int(w/2), N-int(w/2), d):
        small_sample = samples[i-int(w/2):i+int(w/2)]  # fenetre courante
        pos = list(range(i-int(w/2), i+int(w/2)))
        slope = np.polyfit(pos, small_sample, 1)[k]
        slopes.append(slope)
    abs_slopes = []
    n = len(slopes)
    for j in range(n-w-1):
        abs_slopes.append(np.abs(slopes[j+1+w]-slopes[j]))
        # Calcul de la derivee discrete des pentes
    pos_slopes = [i+w for i in range(len(abs_slopes))]
    return abs_slopes, pos_slopes

s_and_w = [[0.1, 100], [0.1, 200], [0.1, 300],[1, 100], [0.5, 100], [0.5, 200]]
show = True
save = False

for sw in s_and_w:
    std, w = sw[0], sw[1]

    plt.figure(figsize=(12, 6))
    plt.suptitle(
        'Linear modelisation with std noise = {0} and window length = {1} '.format(std, w))

    # Tracé des ruptures
    avg_samples = signals.break_avg(std, 3000)
    slope_samples = signals.break_slopes(std, 2000)
    intermittent_samples = signals.break_intermittent(std, N=2000)

    plt.subplot(3, 2, 1)
    plt.plot(avg_samples)
    plt.xlabel('time')
    plt.ylabel('signal')

    plt.subplot(3, 2, 3)
    plt.plot(slope_samples)
    plt.xlabel('time')
    plt.ylabel('signal')

    plt.subplot(3, 2, 5)
    plt.plot(intermittent_samples)
    plt.xlabel('time')
    plt.ylabel('signal')

    # Tracé des caractéristiques
    slopes, pos_slopes = linear(avg_samples, w)

    plt.subplot(3, 2, 2)
    plt.plot(pos_slopes, slopes)
    plt.xlabel('time')
    plt.ylabel('feature')

    slopes, pos_slopes = linear(slope_samples, w)
    plt.subplot(3, 2, 4)
    plt.plot(pos_slopes, slopes)
    plt.xlabel('time')
    plt.ylabel('feature')

    slopes, pos_slopes = linear(intermittent_samples, w)
    plt.subplot(3, 2, 6)
    plt.plot(pos_slopes, slopes)
    plt.xlabel('time')
    plt.ylabel('feature')

    std = str(std)
    std = std.replace('.', '')

    if save:
        plt.savefig('images/modelisations/modelisation_linear/modelisation_linear2_std_' + str(std)+'_w_'+str(w)+'.png')

    if show:
        plt.show()

    print("s : ", std)
    print("w : ", w)