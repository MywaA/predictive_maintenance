import scipy
import numpy as np
import matplotlib.pyplot as plt
import nitime
import audiolazy

import signals

# fig 3.18 à 3.23

def no_avg(L):
    m=0
    for e in L:
        m+=e
    m/=len(L)
    return np.array(L)-m

def AR_modelisation_nitime(samples,w=100,d=1):
  p=2
  N=len(samples)
  t_caracs=[]
  caracs=[]
  for t in range(w,N-w,d):
    #Calcul des fenetres et de sigma
    fen1=no_avg(samples[t-w:t+w])
    [ak3, sig_sq1] = nitime.algorithms.autoregressive.AR_est_LD(fen1, p, rxx=None)
    fen2=no_avg(samples[t-w:t])
    [ak1, sig_sq2] = nitime.algorithms.autoregressive.AR_est_LD(fen2, p, rxx=None)
    fen3=no_avg(samples[t:t+w])
    [ak2, sig_sq3] = nitime.algorithms.autoregressive.AR_est_LD(fen3, p, rxx=None)
    #Test de Brandt
    carac = 0.25*np.abs(2*w*np.log(sig_sq1) - w*np.log(sig_sq2) - w*np.log(sig_sq3))
    caracs.append(carac)
    t_caracs.append(t)
  return caracs,t_caracs

def AR_modelisation_audiolazy(signal,w,p=2):
  t_caracs=[]
  caracs=[]
  t=w
  while t+w<len(signal):
    fen2=no_avg(signal[t-w:t])
    acdata1 = audiolazy.acorr(fen2)
    ldfilt1 = audiolazy.levinson_durbin(acdata1, p)
    sig_sq2=ldfilt1.error

    fen3=no_avg(signal[t:t+w])
    acdata2 = audiolazy.acorr(fen3)
    ldfilt2 = audiolazy.levinson_durbin(acdata2, p)
    sig_sq3=ldfilt2.error

    fen1=no_avg(signal[t-w:t+w])
    acdata3 = audiolazy.acorr(fen1)
    ldfilt3 = audiolazy.levinson_durbin(acdata3, p)
    sig_sq1=ldfilt3.error

    carac = np.abs(2*w*np.log(np.sqrt(np.sqrt(sig_sq1))) - w*np.log(np.sqrt(np.sqrt(sig_sq2))) - w*np.log(np.sqrt(np.sqrt(sig_sq3))))

    caracs.append(carac)
    t_caracs.append(t)
    t+=1

  return caracs,t_caracs

def no_avg(L):
    m=0
    for e in L:
        m+=e
    m/=len(L)
    return np.array(L)-m

s_and_w = [[0.1,100],[0.1,200],[0.1,300],[0.5,100],[1,100]]
show = True
save = False

for sw in s_and_w :
  std,w = sw[0],sw[1]

  plt.figure(figsize=(12,6))
  plt.suptitle('AR modelisation with std noise = {0} and window length = {1}'.format(std,w))

  # Tracé des ruptures
  avg_samples = signals.break_avg(std, 3000)
  slope_samples = signals.break_slopes(std, 2000)
  intermittent_samples = signals.break_intermittent(std, N=2000)

  plt.subplot(3,2,1)
  plt.plot(avg_samples)
  plt.xlabel('time')
  plt.ylabel('signal')

  plt.subplot(3,2,3)
  plt.plot(slope_samples)
  plt.xlabel('time')
  plt.ylabel('signal')

  plt.subplot(3,2,5)
  plt.plot(intermittent_samples)
  plt.xlabel('time')
  plt.ylabel('signal')

  # Tracé des caractéristiques
  caracs,t_caracs = AR_modelisation_nitime(avg_samples,w)

  plt.subplot(3,2,2)
  plt.plot(t_caracs,caracs)
  plt.xlabel('time')
  plt.ylabel('feature')

  caracs,t_caracs = AR_modelisation_nitime(slope_samples,w)

  plt.subplot(3,2,4)
  plt.plot(t_caracs,caracs)
  plt.xlabel('time')
  plt.ylabel('feature')

  caracs,t_caracs = AR_modelisation_nitime(intermittent_samples,w)

  plt.subplot(3,2,6)
  plt.plot(t_caracs,caracs)
  plt.xlabel('time')
  plt.ylabel('feature')

  std=str(std)
  std=std.replace('.', '')

  if save :
    plt.savefig('images/modelisations/modelisation_AR/modelisation_AR_std_'+str(std)+'_w_'+str(w)+'.png')

  if show :
    plt.show()
  
  print("s : ", std)
  print("w : ", w)