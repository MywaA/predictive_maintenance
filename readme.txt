!! Certaines figures sur le rapport déposé sur Edunao ne sont pas les bonnes. Une version à jour du rapport se trouve dans le dépôt Gitlab : Rapport.pdf !!

Voici les emplacements des codes pour générer les tables et figures du rapport :

fig 2.1, 2.2, 2.3 : preprocessing/plot_avg_std_ext.py

fig 3.1 : breaks/break_avg.py
fig 3.2 : breaks/break_slope.py
fig 3.3 : breaks/break_intermittent.py

fig 3.4 à 3.9 : modelisations/modelisation_mean.py
fig 3.10 à 3.17 : modelisations/modelisation_linear.py
fig 3.18 à 3.23 : modelisations/modelisation_AR.py

tables 3.1 à 3.6 : decision/threshold.py (lignes 162-164)

fig 3.24 et table 3.7, fig 3.25 et table 3.8, fig 3.26 et table 3.9 : decision/threshold.py (lignes 200-202)

fig 3.27 à 3.35 : decision/threshold.py (lignes 310-312)

fig 3.36 : prediction/modelisation_mean.py (ligne 211)
fig 3.37 : prediction/modelisation_linear.py (ligne 208)
fig 3.38 : prediction/modelisation_AR.py (ligne 218)

fig 3.39 : prediction/modelisation_mean.py (ligne 214)
fig 3.40 : prediction/modelisation_linear.py (ligne 211)
fig 3.41 : prediction/modelisation_AR.py (ligne 221)

fig 4.1 à 4.3 : echantillonnage/sans_bruit/echantillonnage_brutal.py pour générer les sons, puis spectre avec Audacity
fig 4.4 à 4.6 : echantillonnage/sans_bruit/filtre_moyenneur.py pour générer les sons, puis spectre avec Audacity

fig 4.7 à 4.9 : echantillonnage/avec_bruit/echantillonnage_brutal.py pour générer les sons, puis spectre avec Audacity
fig 4.10 à 4.11 : echantillonnage/avec_bruit/filtre_moyenneur.py pour générer les sons, puis spectre avec Audacity

fig 4.12 à 4.22 : preprocessing/echantillonnage.py

fig 4.23, 4.24, 4.27, 4.28, 4.29, 4.30 : signaux_reels/visualisation/modelisation_mean.py
fig 4.25, 4.26 : signaux_reels/visualisation/modelisation_linear.py

fig 4.31 à 4.40 : signaux_reels/prediction.py puis jouer avec les paramètres pour obtenir les données permettant de tracer les figures