import numpy as np
import nitime

def AR_modelisation_nitime(signal,w,p=2):
  t_caracs=[]
  caracs=[]
  t=w
  while t+w<len(signal):
    fen1=no_avg(signal[t-w:t])
    [ak1, sig_sq1] = nitime.algorithms.autoregressive.AR_est_LD(fen1, p, rxx=None)
    
    fen2=no_avg(signal[t:t+w])
    [ak2, sig_sq2] = nitime.algorithms.autoregressive.AR_est_LD(fen2, p, rxx=None)

    fen3=no_avg(signal[t-w:t+w])
    [ak3, sig_sq3] = nitime.algorithms.autoregressive.AR_est_LD(fen3, p, rxx=None)

    carac = np.abs(2*w*np.log(np.sqrt(np.sqrt(sig_sq3))) - w*np.log(np.sqrt(np.sqrt(sig_sq1))) - w*np.log(np.sqrt(np.sqrt(sig_sq2))))

    caracs.append(carac)
    t_caracs.append(t)
    t+=1

  return caracs,t_caracs

def no_avg(L):
    m=0
    for e in L:
        m+=e
    m/=len(L)
    return np.array(L)-m