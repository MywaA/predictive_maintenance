import modelisation_mean
import modelisation_linear
import modelisation_AR
import signals
import numpy as np
import matplotlib.pyplot as plt



### Help functions

def lowest_values(samples, p=0.9):
    n = len(samples)
    samp = samples.copy()  # On ne veut pas modifier la série
    samp.sort()
    k = min(int(p*n), n-1)  # On ne prendra pas toute la liste
    smallest = samp[0:k]
    return smallest


def estimated_mean(sample):
    est_mean = 0
    N = len(sample)
    for i in sample:
        est_mean += i
    return est_mean/N


def estimated_std(sample):
    est_mean = estimated_mean(sample)
    N = len(sample)
    sum_squares = 0
    for i in sample:
        sum_squares += i**2
    est_std = sum_squares/N - est_mean**2
    return np.sqrt(est_std)


def close_to_list(a, L, e):
    close = True
    for b in L:
        close = close and np.abs(a-b) <= e
    return close

# --------------------------



### 3 - Prise de décision

# Estimate the correct break times
def break_times_est(feature, pos, p, method="three", detection_type="mean"):
    # Definition du seuil (threshold)
    feature_sub = lowest_values(feature, p)  # lowest p%
    est_std = estimated_std(feature_sub)  # estimation globale sigma
    est_mean = estimated_mean(feature_sub)
    if method == "three":
        threshold = 3*est_std + est_mean
    elif method == "universal":
        threshold = np.sqrt(2*np.log(len(feature)))*est_std + est_mean

    break_times = []
    # Detection de la rupture premier depassement
    if detection_type == "first":
        last_break_time = pos[0]
        for i in range(len(pos)):
            if feature[i] >= threshold:  # Si on depasse le seuil
                if last_break_time != pos[i-1]:
                    break_times.append(pos[i])
                last_break_time = pos[i]
    # Detection de la rupture milieu pic
    elif detection_type == "mean":
        small_break = []  # Liste qui contient l'ensemble des points du dépassement actuel
        for i in range(len(pos)):
            if feature[i] >= threshold:  # Si on depasse le seuil
                small_break.append(pos[i])
            else:
                if len(small_break) > 5:  # Il faut au moins 5 points consecutifs pour valider un pic
                    failure = estimated_mean(small_break)
                    small_break = []
                    break_times.append(failure)
                elif small_break != []:
                    small_break = []
    # Detection de la rupture dernier depassement
    elif detection_type == "last":
        small_break = []
        for i in range(len(pos)):
            if feature[i] >= threshold:  # Si on depasse le seuil
                small_break.append(pos[i])
            else:
                if small_break != []:
                    failure = small_break[-1]
                    small_break = []
                    break_times.append(failure)
    return break_times

# Simulate the behavior with only one sensor
def simulate(std=0.1, w=100, K=100, n=10, ps=[0.9, 0.95, 0.98], break_type="avg"):
    good_detection = 0  # Nombre de detections correctes
    bad_detection = 0  # Nombre de fausses alertes
    for k in range(K):  # Nombre de signaux à générer
            # Detection d'une rupture de moyenne
        if break_type == "avg":
            samples = signals.break_avg(std, N=3000)
            feature, pos = modelisation_mean.mean(samples, w, 1)
            # Utilisation de la modelisation par les moyennes
            param = 2  # Nombre de ruptures a detecter
            p = ps[0]
            failures = break_times_est(
                feature, pos, p, method="three", detection_type="mean")
        # Detection d'une rupture de pente
        elif break_type == "slope":
            samples = signals.break_slopes(std, N=2000)
            feature, pos = modelisation_linear.linear(samples, w=w, k=0)
            # Utilisation de la modelisation par des troncons affine
            param = 1
            p = ps[1]
            failures = break_times_est(
                feature, pos, p, method="three", detection_type="mean")
        # Detection d'une rupture intermittente
        elif break_type == "intermittent":
            samples = signals.break_intermittent(std, N=2000)
            feature, pos = modelisation_AR.AR_modelisation_nitime(
                samples, w, p=2)
            # Utilisation de la modelisation par des processsus AR
            param = 1
            p = ps[2]
            failures = break_times_est(
                feature, pos, p, method="three", detection_type="last")
        current_good_detection = 0
        for f in failures:
                    # On parcourt toutes les ruptures detectees
            if np.abs(f-1000) <= n or (break_type == "avg" and np.abs(f-2000) <= n):
                # Si on se situe a une veritable rupture
                current_good_detection += 1
            else:
                bad_detection += 1
        good_detection += min(current_good_detection, param)
        # Si on detecte current_good_detection > param alors on a compte 2 fois une detection correcte
        # Cela ne fausse pas la modelisation car on trigger 2 fois sur un signal problematique
    return good_detection, bad_detection

def stat_simulate(s_and_w,n,ps,K,break_type):
    M = len(s_and_w)
    i = 0
    for sw in s_and_w:
        i += 1
        print(i, " / ", M)
        good, bad = simulate(std=sw[0], w=sw[1], K=K,n=n, ps=ps, break_type=break_type)
        print("Break type : {}".format(break_type))
        print("σ² : {0}, w : {1}".format(round(sw[0]**2, 2), sw[1]))
        print("Correct detections : {}".format(good))
        if break_type == "avg":
            print("Missed detections : {}".format(K*2-good))
        else:
            print("Missed detections : {}".format(K-good))
        print("False detections : {}".format(bad))
        print("")

s_and_w = [[0.2, 100], [0.2, 200], [0.2, 300], [0.4, 100], [0.4, 200], [0.4, 300]]

stat_simulate(s_and_w,n=10,ps=[0.9, 0.95, 0.98],K=150,break_type="avg") # Tables 3.1,3.2
stat_simulate(s_and_w,n=30,ps=[0.9, 0.95, 0.98],K=100,break_type="slope") # Tables 3.3,3.4
stat_simulate(s_and_w,n=10,ps=[0.9, 0.95, 0.98],K=100,break_type="intermittent") # Tables 3.5,3.6

# --------------------------



### 4 - Robustesse au bruit

def robustesse_bruit(S, w, K=1000, n=10, ps=[0.9, 0.94, 0.98], break_type="avg", save=False, show=False):
    good_rate = []
    bad_rate = []
    for s in S:
        print(s)
        good, bad = simulate(std=s, w=w, K=K, n=n, ps=ps,
                             break_type=break_type)
        print(good,bad,good+bad)
        if break_type == "avg":
            good_rate.append(good/(2*K))
            bad_rate.append(bad/(good+bad))
        else:
            good_rate.append(good/(K))
            bad_rate.append(bad/(good+bad))
    # Plot
    plt.rcParams["figure.figsize"] = [16, 9]
    fig, ax = plt.subplots(2, 1)
    ax[0].plot(S, good_rate)
    ax[0].set_xlabel('σ')
    ax[0].set_ylabel('Taux bonne détection')
    ax[1].plot(S, bad_rate)
    ax[1].set_xlabel('σ')
    ax[1].set_ylabel('Taux fauses alarmes')

    if save :
      plt.savefig('images/decision/robustesse_bruit/robustesse_bruit_'+break_type+'_w_'+str(w)+'_n_'+str(n)+'.png')
    if show:
        plt.show()

robustesse_bruit([0.1,0.3,0.4,0.5,0.7,0.81,1,1.2],w=100,K=1000,n=10,ps=[0.9,0.94,0.98],break_type="avg",save=False,show=True) # fig 3.24 et table 3.7
robustesse_bruit([0.1,0.3,0.4,0.5,0.7,0.81,1,1.2],w=200,K=1000,n=30,ps=[0.9,0.94,0.98],break_type="slope",save=False,show=True) # fig 3.25 et table 3.8
robustesse_bruit([0.1,0.3,0.4,0.5,0.7,0.81,1,1.2],w=100,K=1000,n=10,ps=[0.9,0.94,0.98],break_type="intermittent",save=False,show=True) # fig 3.26 et table 3.9

# --------------------------



### 5 - Signaux multivariés

# Compute failure detection for v_i
def v(i, failures, m):
    vote = []
    threshold = {1: m, 2: m-1, 3: (m//2)+1}
    for v in failures:
        if v >= threshold[i]:
            vote.append(1)
            # Le vote est accepte
        else:
            vote.append(0)
            # Le vote est refuse
    return vote

# Simulate the behavior with m sensors
def simulate_multi_variable(m, std, w, K, n, ps=[0.9, 0.94, 0.98], break_type="avg"):
    total_real_failures = 0  # Nombre total de ruptures a detecter
    real_failures_detected_v1 = 0  # Vraies rupturse detectees
    real_failures_detected_v2 = 0
    real_failures_detected_v3 = 0
    detected_v1 = 0  # Total des ruptures detectees
    detected_v2 = 0
    detected_v3 = 0
    for k in range(K):  # Generation de K signaux
        total_failures = []  # la liste des listes de ruptures
        for i in range(m):
            if break_type == "avg":
                samples = signals.break_avg(std, N=3000)
                feature, pos = modelisation_mean.mean(samples, w, 1)
                param = 2  # Nombre de rupture pour ce signal
                p = ps[0]
                failures = break_times_est(
                    feature, pos, p, method="three", detection_type="mean")
            elif break_type == "slope":
                samples = signals.break_slopes(std, N=2000)
                feature, pos = modelisation_linear.linear(samples, w=w, k=0)
                param = 1  # Nombre de rupture pour ce signal
                p = ps[1]
                failures = break_times_est(
                    feature, pos, p, method="three", detection_type="mean")
            elif break_type == "intermittent":
                samples = signals.break_intermittent(std, N=2000)
                feature, pos = modelisation_AR.AR_modelisation_nitime(
                    samples, w, p=2)
                param = 1  # Nombre de rupture pour ce signal
                p = ps[2]
                failures = break_times_est(
                    feature, pos, p, method="three", detection_type="last")
            total_failures.append(failures)
        total_real_failures += param
        total_failures = sorted(total_failures, key=lambda L: len(
            L), reverse=True)  # Tri par nombre de rupture detectee
        detected_failures = [[f] for f in total_failures[0]]
        for i in range(1, len(total_failures)):
            for f in total_failures[i]:
                k = 0
                while k < len(detected_failures):
                    # On recherche si f appartient a une liste de ruptures
                    if close_to_list(f, detected_failures[k], n):
                        detected_failures[k].append(f)
                        k = len(detected_failures)
                    k += 1
        failures_mean = [estimated_mean(f) for f in detected_failures]
        # On peut calculer la valeur moyenne de la rupture quand on a plus besoin de la distribution
        failures_vote = [len(f) for f in detected_failures]
        # Le nombre de votes par rupture
        # On regarde si le vote est accepte pour les 3 scrutins
        v1 = v(1, failures_vote, m)
        v2 = v(2, failures_vote, m)
        v3 = v(3, failures_vote, m)
        real_failures_detected_v1 += sum([1 for k in range(len(failures_mean)) if v1[k] == 1 and (
            np.abs(failures_mean[k]-1000) <= n or np.abs(failures_mean[k]-2000) <= n)])
        real_failures_detected_v2 += sum([1 for k in range(len(failures_mean)) if v2[k] == 1 and (
            np.abs(failures_mean[k]-1000) <= n or np.abs(failures_mean[k]-2000) <= n)])
        real_failures_detected_v3 += sum([1 for k in range(len(failures_mean)) if v3[k] == 1 and (
            np.abs(failures_mean[k]-1000) <= n or np.abs(failures_mean[k]-2000) <= n)])
        # On regarde si la rupture etait vraie en fonction de sa position
        detected_v1 += sum(v1)
        detected_v2 += sum(v2)
        detected_v3 += sum(v3)
        # On calcule toutes les ruptures detectees
    good_detection_v1 = real_failures_detected_v1
    bad_detection_v1 = detected_v1-real_failures_detected_v1
    good_detection_v2 = real_failures_detected_v2
    bad_detection_v2 = detected_v2-real_failures_detected_v2
    good_detection_v3 = real_failures_detected_v3
    bad_detection_v3 = detected_v3-real_failures_detected_v3
    # On a alors les nombres de bonnes et mauvaises detections
    return (good_detection_v1, bad_detection_v1, good_detection_v2, bad_detection_v2, good_detection_v3, bad_detection_v3)

def stat_multi(S, M, K, break_type, w, n):
    for m in M:
        for s in S:
            results = simulate_multi_variable(m, s, w, K, n, break_type=break_type)
            print("Break type :", break_type)
            print("m :", m)
            print("std :", s)
            print("(good_detection_v1, bad_detection_v1, good_detection_v2, bad_detection_v2, good_detection_v3, bad_detection_v3) :",results)
            print("---")
      
stat_multi(S=[0.1,0.3,0.4,0.5,0.7,0.8,1,1.2],M=[5,10],K=1000,break_type="avg",w=100,n=10) # données pour fig 3.27, 3.28, 3.29
stat_multi(S=[0.1,0.3,0.4,0.5,0.7,0.8,1,1.2],M=[5,10],K=100,break_type="slope",w=200,n=30) # données pour fig 3.30, 3.31, 3.32
stat_multi(S=[0.1,0.3,0.4,0.5,0.7,0.8,1,1.2],M=[5,10],K=100,break_type="intermittent",w=100,n=10) # données pour fig 3.33, 3.34, 3.35

# --------------------------