import numpy as np

def linear(samples,w=100,k=0):
    N=len(samples)
    slopes=[]
    #k=1 -> mean
    #k=0 -> slope
    for i in range(int(w/2),N-int(w/2)):
        small_sample = samples[i-int(w/2):i+int(w/2)]
        pos = list(range(i-int(w/2),i+int(w/2)))
        slope=np.polyfit(pos,small_sample,1)[k]
        slopes.append(slope)
    abs_slopes=[]
    n=len(slopes)
    for j in range(n-w-1):
        abs_slopes.append(np.abs(slopes[j+1+w]-slopes[j]))
    pos_slopes=[i+w for i in range(len(abs_slopes))]
    return abs_slopes, pos_slopes