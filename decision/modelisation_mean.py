import numpy as np

def mean(signal,w,d):
  t_means=[]
  means=[]
  t=w
  while t<len(signal):
    sigW=signal[t-w:t]
    sigW_mean=0
    n = len(sigW)
    for i in range(n):
      sigW_mean+=sigW[i]
    means.append(sigW_mean/n)
    t_means.append(t)
    t+=d

  diff_means=[]
  for k in range(len(means)-(w//d + 1)):
    diff_means.append(np.abs(means[k+w//d+1]-means[k]))
    
  return diff_means,t_means[0:len(means)-(w//d+1)]