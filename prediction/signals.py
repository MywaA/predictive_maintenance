import numpy as np

# Ruptures

def break_avg(std,N=2000):
    mean_low=0
    mean_high=3
    samples1=np.random.normal(mean_low,std,size=N//2)
    samples2=np.random.normal(mean_high,std,size=N//2)
    samples=np.concatenate((samples1,samples2),axis=0)
    return samples

def break_slopes(std,N=2000):
    N=2000
    slope=0.005
    mean_left=0
    x1=[mean_left+np.random.normal(0,std,) for i in range(N//2)]
    x2=[mean_left+i*slope+np.random.normal(0,std,) for i in range(N//2)]
    return np.concatenate((x1,x2),axis=0)

def break_intermittent(std,N=2000):
    horizon = int(N)
    points = int(N/2)
    # partie 1
    mean1 = 0
    std1 = 0.1
    samples = np.random.normal(mean1,std1,size=points)

    # parties suivantes
    mean2 = 3 # utilisé pour choisir aléatoirement les valeurs sur le palier élevé
    std2 = 0.1
    mean3 = 50 # utilisé pour choisir aléatoirement les durées de chaque palier (subOneDuration et subTwoDuration)
    std3 = 15

    t=points+1
    while t<horizon :
        subOneDuration = np.abs(np.floor(np.random.normal(mean3 , std3)))
        t+= subOneDuration
        samples1 = np.random.normal(mean2, std2, size=int(subOneDuration))
        samples = np.concatenate ((samples, samples1) , axis = 0)

        subTwoDuration = np.abs(np.floor(np.random.normal(mean3 , std3)))
        t+= subTwoDuration
        samples2 = np.random.normal(mean1, std1, size=int(subTwoDuration))
        samples = np.concatenate ((samples, samples2) , axis = 0)
    return samples[0:2*points]