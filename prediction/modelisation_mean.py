import numpy as np
import matplotlib.pyplot as plt
import signals

def break_times_est(feature,pos,factor,w=100) :
    # calculation of threshold
    sigma,mean = sigma_samples(feature)
    threshold = factor*sigma + mean

    # detection of break
    break_times = []
    last_break_time = pos[0]
    for i in range(w,len(pos)) :
        if feature[i] >= threshold[i] :
            if last_break_time != pos[i-1] :
                break_times.append(pos[i])
            last_break_time = pos[i]
    return break_times

def mean_modelisation(signal,w,d):
  t_means=[]
  means=[]
  t=w
  while t<len(signal):
    sigW=signal[t-w:t]
    sigW_mean=0
    n = len(sigW)
    for i in range(n):
      sigW_mean+=sigW[i]
    means.append(sigW_mean/n)
    t_means.append(t)
    t+=d

  diff_means=[]
  for k in range(2*w,len(signal)):
    diff_means.append(np.abs(means[k-w]-means[k-2*w]))

  return diff_means,t_means[w:len(means)]

def sigma_samples(samples):
    N = len(samples)
    sigma = []
    mean = [samples[0]]
    mean_sq = [(samples[0])**2]
    for i in range(1, N):
        mean.append((mean[-1]*i+samples[i])/(i+1))
        mean_sq.append((mean_sq[-1]*i+samples[i]**2)/(i+1))
    sigma = []
    for i in range(N):
        sigma.append(np.sqrt(mean_sq[i]-mean[i]**2))
    return np.array(sigma), np.array(mean)

color = ['#377eb8', '#ff7f00', '#4daf4a','#f781bf', '#a65628', '#984ea3','#999999', '#e41a1c', '#dede00']


def display(factors,s_and_w,save=False) :
  for sw in s_and_w :
    std,w = sw[0],sw[1]

    plt.figure(figsize=(12,6))
    plt.suptitle('Mean modelisation with std noise = {0} and window length = {1}'.format(std,w))

    ## Tracé des ruptures
    avg_samples=signals.break_avg(std,2000)
    slope_samples = signals.break_slopes(std,2000)
    intermittent_samples=signals.break_intermittent(std,N=2000)
    
    plt.subplot(3,2,1)
    plt.plot(avg_samples)
    plt.xlabel('Time')
    plt.ylabel('Signal')

    plt.subplot(3,2,3)
    plt.plot(slope_samples)
    plt.xlabel('Time')
    plt.ylabel('Signal')

    plt.subplot(3,2,5)
    plt.plot(intermittent_samples)
    plt.xlabel('Time')
    plt.ylabel('Signal')

    ## Tracé des caractéristiques
    slopes,pos_slopes = mean_modelisation(avg_samples,w,1)

    plt.subplot(3,2,2)
    plt.plot(pos_slopes,slopes)
    sigma,mean = sigma_samples(slopes)
    i=1
    for f in factors :
      plt.plot(pos_slopes,f*sigma+mean,label="seuil = {}σ + µ".format(f),color=color[i%len(factors)])
      i+=1
    plt.legend()
    plt.xlabel('Time')
    plt.ylabel('Feature')
    

    slopes,pos_slopes = mean_modelisation(slope_samples,w,1)

    plt.subplot(3,2,4)
    plt.plot(pos_slopes,slopes)
    sigma,mean = sigma_samples(slopes)
    i=1
    for f in factors :
      plt.plot(pos_slopes,f*sigma+mean,label="seuil = {}σ + µ".format(f),color=color[i%len(factors)])
      i+=1
    plt.legend()
    plt.xlabel('Time')
    plt.ylabel('Feature')
    

    slopes,pos_slopes = mean_modelisation(intermittent_samples,w,1)

    plt.subplot(3,2,6)
    plt.plot(pos_slopes,slopes)
    sigma,mean = sigma_samples(slopes)
    i=1
    for f in factors :
      plt.plot(pos_slopes,f*sigma+mean,label="seuil = {}σ + µ".format(f),color=color[i%len(factors)])
      i+=1
    plt.legend()
    plt.xlabel('Time')
    plt.ylabel('Feature')

    if save :
        std=str(std)
        std=std.replace('.', '')
        plt.savefig('images/prediction/seuils/modelisation_mean/modelisation_mean_std_'+str(std)+'_w_'+str(w)+'.png')
    plt.show()

def stat(factors,K=100,std=0.1,w=100,save=False):
    max_delay=[]
    min_delay=[]
    mean_delay=[]
    correct_detection = []
    false_alarm = []

    for f in factors :
        print(f)
        delay_simul=[]
        not_detected_simul = 0
        false_alarm_simul = 0
        for k in range(K):
            mean_samples = signals.break_avg(std,2000)
            diff_means,pos = mean_modelisation(mean_samples,w,d=1)
            break_time = break_times_est(diff_means,pos,f,w=w)
            if break_time==[]:
                not_detected_simul +=1
            else:
                b=True
                for e in break_time:
                    if b :
                        if e<1000:
                            false_alarm_simul+=1
                        else:
                            delay_simul.append(e-1000)
                            b=False
                    else : break

        try:
            max_delay.append(max(delay_simul))
        except:
            max_delay.append(0)
        try:
            min_delay.append(min(delay_simul))
        except:
            min_delay.append(1000)
        mean_delay.append(np.mean(delay_simul))
        
        correct_detection.append((K-not_detected_simul)/K)
        if K + false_alarm_simul != not_detected_simul :
            false_alarm.append(false_alarm_simul/(K-not_detected_simul+false_alarm_simul))
        else :
            false_alarm.append(false_alarm_simul/(K))
    
    plt.figure(figsize=(12,6))

    plt.subplot(3,2,1)
    plt.plot(factors,correct_detection)
    plt.xlabel("Facteur")
    plt.ylabel("Taux de ruptures détectées")

    plt.subplot(3,2,2)
    plt.plot(factors,false_alarm)
    plt.xlabel("Facteur")
    plt.ylabel("Taux de fausses alarmes")

    plt.subplot(3,2,3)
    plt.plot(factors,max_delay)
    plt.xlabel("Facteur")
    plt.ylabel("Délai max")

    plt.subplot(3,2,4)
    plt.plot(factors,min_delay)
    plt.xlabel("Facteur")
    plt.ylabel("Délai min")

    plt.subplot(3,2,5)
    plt.plot(factors,mean_delay)
    plt.xlabel("Facteur")
    plt.ylabel("Délai moyen")

    plt.suptitle("Détection rupture de moyenne - Modèle de la moyenne")
    if save:
            plt.savefig('images/prediction/factor_changing_avg_mean_std_02_w_100.png')
    plt.show()


factors1 = [2,4,8]
s_and_w = [[0.2,100]]
display(factors1,s_and_w,save=False) # fig 3.36

factors2 = np.array(range(6,16,1))/2
stat(factors2,K=200,std=0.2,w=100,save=False) # fig 3.39