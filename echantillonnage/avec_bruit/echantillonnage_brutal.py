import wave
import numpy as np

# fig 4.7 à 4.9 (N=1,15,20)
N = 1 # facteur de sous-échantillonnage

nomfichier = 'sons/avec_bruit/ech_brutal/son_'+str(N)+'_brutal.wav'
monson = wave.open(nomfichier, 'wb')

ncanal = 1  # mono
noctet = 2  # taille d'un échantillon
fe = int(16000/N)  # fréquence d'échantillonnage (en Hz)
duree = 2  # duree du son (en s)
f0 = 440  # frequence de la sinusoide (en Hz)

nechantillon = int(duree*fe)

# contenu de l'en tete
parametres = (ncanal, noctet, fe, nechantillon, 'NONE', 'notcompressed')
# creation de l'en tete (44 octets)
monson.setparams(parametres)

# phase a l'origine aleatoire entre 0 et 2pi
phaz = 2*np.pi*np.random.uniform(0, 1, size=1)

## DIFFERENCE PAR RAPPORT AU CODE SANS BRUIT
#-------------------------------------------
# bruit
mean = 0
std = 0.1
noise = np.random.normal(mean, std, size=nechantillon)
# on cree le signal
val = []
for i in range(0, nechantillon):
    if noctet == 1:
        val.append(int(128.0+127.0*(np.sin(2.0*np.pi*f0*i/fe+phaz)+noise[i])))
    if noctet == 2:
        val.append(int(32767.0*(np.sin(2.0*np.pi*f0*i/fe+phaz)+noise[i])))
# on norme le signal
maxi = max(np.abs(val))
norm = 32767.0/maxi
for i in range(len(val)):
    val[i] = int(val[i]*norm)
#-------------------------------------------

# on le convertit en"son"
signal = []
for i in range(0, nechantillon):
    if noctet == 1:
        # <: little endian ; B: unsigned char (1 octet)
        signal = wave.struct.pack('<B', val[i])
    if noctet == 2:
        # <: little endian ; h: short int (2 octets)
        signal = wave.struct.pack('<h', val[i])
    monson.writeframes(signal)  # ecriture de l'échantillon sonore courant
monson.close()
