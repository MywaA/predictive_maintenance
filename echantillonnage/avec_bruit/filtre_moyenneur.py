import wave
import numpy as np

# fig 4.10 à 4.11 (N=15,20)
N = 20  # facteur de sous-echantillonnage

nomfichier = 'sons/avec_bruit/filtre_moyenneur/son_'+str(N)+'_fm.wav'
monson = wave.open(nomfichier, 'wb')

ncanal = 1  # mono
noctet = 2  # taille d'un echantillon
fe = 16000  # frequence d'echantillonnage du son originel
fe_se = int(fe/N)  # fréquence d'echantillonnage apres sous-echantillonnage
duree = 2  # duree du son (en s)
f0 = 440  # frequence de la sinusoide (en Hz)

nechantillon = int(duree*fe) # nb d'echantillons avant sous-echantillonnage
nechantillon_se = int(duree*fe_se) # nb d'echantillons apres sous-echantillonnage

# contenu de l'en tete
parametres = (ncanal, noctet, fe_se, nechantillon_se, 'NONE', 'notcompressed')

# creation de l'en tete (44octets)
monson.setparams(parametres)

# phase a l'origine aleatoire entre 0 et 2pi
phaz = 2*np.pi*np.random.uniform(0, 1, size=1)


## DIFFERENCE PAR RAPPORT AU CODE SANS BRUIT
#-------------------------------------------
# bruit
mean = 0
std = 0.1
noise = np.random.normal(mean, std, size=nechantillon)
# on cree le signal (echantillonné a fe)
val = []
for i in range(0, nechantillon):
    if noctet == 1:
        val.append(int(128.0+127.0*(np.sin(2.0*np.pi*f0*i/fe+phaz)+noise[i])))
    if noctet == 2:
        val.append(int(32767.0*(np.sin(2.0*np.pi*f0*i/fe+phaz)+noise[i])))
#-------------------------------------------

# filtre moyenneur (sous-echantillonnage)
val_ech = []
i = 0
while i+N <= len(val):
    # calcul de la moyenne sur les N echantillons
    m = 0
    ech = val[i:i+N]
    for j in ech:
        m += j
    val_ech.append(int(m/len(ech)))
    i += N

# on norme le signal sous-echantillonne
maxi = max(np.abs(val_ech))
norm = 32767.0/maxi
for i in range(len(val_ech)):
    val_ech[i] = int(val_ech[i]*norm)

# on le convertit en "son"
signal = []
for i in range(0, nechantillon_se):
    if noctet == 1:
        # <: little endian ; B: unsigned char (1 octet)
        signal = wave.struct.pack('<B', val_ech[i])
    if noctet == 2:
        # <: little endian ; h: short int (2 octets)
        signal = wave.struct.pack('<h', val_ech[i])
    monson.writeframes(signal)  # ecriture de l'echantillon sonore courant
monson.close()