import datetime
import re

# Pour un écart mesure/consigne, on récupère les données (échantillonées) contenues dans les fichiers txt (vecteur temps et vecteur des valeurs)


def gap_sampled(numero_variable):
    nom_fichier = 'signaux_reels/data/variables_' + \
        str(numero_variable)+'_'+str(numero_variable+1)+'.txt'
    # Ouverture du fichier en lecture
    try:
        fichier = open(nom_fichier, "r", encoding='utf-8')
    except:
        print('Impossible de lire le fichier', nom_fichier)
        exit()

    lignes = fichier.readlines()
    fichier.close()

    # Lecture et récupération des donnees contenues dans le fichier
    nb_observations = len(lignes)
    x = []
    x_time = []
    compteur_ligne = 0
    for ligne in lignes:
        ligne = re.sub(r"\n", r"", ligne)
        liste_valeur_variables = ligne.split('xx')
        date_object = datetime.datetime.strptime(
            liste_valeur_variables[0], '%Y-%m-%d %H:%M:%S')
        str_date_time = date_object.strftime('%H:%M:%S')
        str_date = date_object.strftime('%d.%m.%Y')
        date_time_object = datetime.datetime.strptime(
            str_date + " " + str_date_time, '%d.%m.%Y %H:%M:%S')
        x_time.append(date_time_object)

        x.append(float(liste_valeur_variables[1]))
        compteur_ligne = compteur_ligne + 1
    return x_time, x