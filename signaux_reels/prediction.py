import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'figure.max_open_warning': 0})

import modelisation_linear_prediction
import modelisation_AR_prediction
import modelisation_mean_prediction

# fig 4.31 à 4.40

# Rupture theorique attendue (en position d'echantillon dans le signal original echantillonne)
rupture_theorique = 880

taille_signal = 960  # = 96000/N avec N=100

# Definition des variables utilisees
zones = {}
for i in range(8):  # 8 zones du four
    num_variables = []
    # Pour chaque zone, on utilise les ecarts de temperature, de debit d'air et de debit de gaz car les deux autres (vannes) sont inutilisables (voir forme des signaux dans signaux_reels>images pour se rendre compte de tout ça). De meme, les deux dernieres variables (pression et position clapet) ne seront pas utilisees.
    for j in [0, 2, 6]:
        num_variables.append(int(str(i)+str(j)))
    zones[i] = num_variables


# Pour chaque zone, on predit des ruptures, qui sont de type differents en fonction de la variable
# 1er couple de variables : Temperature -> rupture de pente -> modele lineaire
# 2eme couple de variables : Debit d'air -> rupture intermittente / de moyenne -> modele AR / des moyennes
# 3eme couple de variables : Debit de gaz -> rupture intermittente / de moyenne -> modele AR / des moyennes

### Parametres

# Definition des parametres pour chaque modele utilise
linear_factor = 5
linear_w = 200
mean_factor = 6
mean_w = 100
AR_factor = 6
AR_w = 100

# Fenetre pour le vote
delta = 10

# Sauvegarder les figures ? Attention il y en a 24 !
save = False
# Afficher les figures ? Attention il y en a 24 !
show = False

#-----------------------


### Code (modelisation des moyennes pour les ruptures de moyenne/intermittentes (variables correspondant aux débits d'air et gaz) et vote 2/3)

print('Rupture theorique :', rupture_theorique)
print('')
print('Parametres : ')
print('Facteur de seuillage pour la modelisation lineaire :', linear_factor)
print('Taille de la fenetre (en echantillons) pour la modelisation lineaire :', linear_w)
print('Facteur de seuillage pour la modelisation des moyennes :', mean_factor)
print('Taille de la fenetre (en echantillons) pour la modelisation des moyennes :', mean_w)
#print('Facteur de seuillage pour la modelisation AR :',AR_factor)
#print('Taille de la fenetre (en echantillons) pour la modelisation AR :',AR_w)
print("Taille de la fenetre pour le vote (en echantillons) :", delta)

total_bonnes = 0
total_mauvaises = 0
delays = []
for i in zones.keys():
    break_times_detected_in_zone = [
        [False for k in range(taille_signal)] for r in range(len(zones[i]))]

    # -- ESTIMATION DES RUPTURES
    # couple_var prend la valeur de la premiere variable du duo des deux variables dont on prend l'ecart
    for couple_var in zones[i]:
        if (str(couple_var)[-1]) == '0':  # Temperatures
            temp_break_times = modelisation_linear_prediction.linear_break_times(
                couple_var, linear_factor, linear_w, save=save)
            for k in range(len(temp_break_times[1])):
                # prise en compte du decalage de la caracteristique afin de pouvoir comparer avec les valeurs theoriques
                temp_break_times[1][k] += 2*linear_w
                break_times_detected_in_zone[0][temp_break_times[1][k]] = True
            # break_times_detected_in_zone[0].append(temp_break_times[1])
        elif (str(couple_var)[-1]) == '2':  # Debit d'air
            #air_break_times = modelisation_AR_prediction.AR_break_times(couple_var, AR_factor, AR_w, save=save)
            # for k in range(len(air_break_times[1])):
                # air_break_times[1][k] += 2*AR_w # prise en compte du decalage de la caracteristique afin de pouvoir comparer avec les valeurs theoriques
                #break_times_detected_in_zone[1][air_break_times[1][k]] = True
            air_break_times = modelisation_mean_prediction.mean_break_times(
                couple_var, mean_factor, mean_w, save=save)
            for k in range(len(air_break_times[1])):
                # prise en compte du decalage de la caracteristique afin de pouvoir comparer avec les valeurs theoriques
                air_break_times[1][k] += 2*mean_w
                break_times_detected_in_zone[1][air_break_times[1][k]] = True

        elif (str(couple_var)[-1]) == '6':  # Debit de gaz
            #gaz_break_times = modelisation_AR_prediction.AR_break_times(couple_var, AR_factor, AR_w, save=save)
            # for k in range(len(air_break_times[1])):
                # air_break_times[1][k] += 2*AR_w # prise en compte du decalage de la caracteristique afin de pouvoir comparer avec les valeurs theoriques
                #break_times_detected_in_zone[2][gaz_break_times[1][k]] = True
            gaz_break_times = modelisation_mean_prediction.mean_break_times(
                couple_var, mean_factor, mean_w, save=save)
            for k in range(len(gaz_break_times[1])):
                # prise en compte du decalage de la caracteristique afin de pouvoir comparer avec les valeurs theoriques
                gaz_break_times[1][k] += 2*mean_w
                break_times_detected_in_zone[2][gaz_break_times[1][k]] = True
    if show :
        plt.show()
    print("")
    print('Ruptures detectees dans la zone {0} : {1}'.format(
        i+1, [[i for i in range(taille_signal) if break_times_detected_in_zone[k][i]] for k in range(3)]))

    # -- VOTE -> CHOIX DU/DES TEMPS DE RUPTURE
    ruptures_validees = []
    rupture_detectee = False
    for j in range(delta, taille_signal):
        if rupture_detectee:
            break
        else:
            fen0 = break_times_detected_in_zone[0][j-delta:j]
            fen1 = break_times_detected_in_zone[1][j-delta:j]
            fen2 = break_times_detected_in_zone[2][j-delta:j]

            true0 = 0
            valid0 = 0
            for ind in range(len(fen0)):
                if fen0[ind]:
                    valid0 = 1
                    true0 = ind + j-delta
                    break
            true1 = 0
            valid1 = 0
            for ind in range(len(fen1)):
                if fen1[ind]:
                    valid1 = 1
                    true1 = ind + j-delta
                    break
            true2 = 0
            valid2 = 0
            for ind in range(len(fen2)):
                if fen2[ind]:
                    valid2 = 1
                    true2 = ind + j-delta
                    break
            # vote majoritaire (2/3 ont detecte une rupture)
            m = max(true0, true1, true2)
            if valid0 + valid1 + valid2 >= 2 and m not in ruptures_validees:
                ruptures_validees.append(m)
                if m >= rupture_theorique:
                    delays.append(m-rupture_theorique)
                    rupture_detectee = True
    print("Rupture(s) validee(s) par le vote majoritaire : ", ruptures_validees)

    # -- EVALUATION DES PERFORMANCES
    bonnes = "Non"
    mauvaises = 0
    for rupt in ruptures_validees:
        if rupt >= rupture_theorique:
            bonnes = "Oui"
        else:
            mauvaises += 1
    if bonnes == "Oui":
        total_bonnes += 1
    total_mauvaises += mauvaises

    print('Rupture theorique bien detectee ?', bonnes)
    print('Nombre de fausses alarmes :', mauvaises)

print("")
print("Synthese sur les 8 zones :")
print("Total de bonnes detections :", total_bonnes, "/", 8)
print("Total de fausses alarmes :", total_mauvaises)
print("Delais de detection (en echantillons de 100 secondes) :",delays)
print("Delai max, min, moyen : {0} ; {1} ; {2}".format(max(delays),min(delays),round(np.mean(delays),2)))
#-----------------------