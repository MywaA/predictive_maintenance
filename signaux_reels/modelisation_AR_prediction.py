import numpy as np
import matplotlib.pyplot as plt
import nitime
import get_sampled_var_data


def no_avg(L):
    m = 0
    for e in L:
        m += e
    m /= len(L)
    return np.array(L)-m


def AR_modelisation_nitime(signal, time, w, p=2):
    t_caracs = []
    caracs = []
    t = 2*w
    while t < len(signal):
        fen1 = no_avg(signal[t-2*w:t-w])
        [ak1, sig_sq1] = nitime.algorithms.autoregressive.AR_est_LD(
            fen1, p, rxx=None)

        fen2 = no_avg(signal[t-w:t])
        [ak2, sig_sq2] = nitime.algorithms.autoregressive.AR_est_LD(
            fen2, p, rxx=None)

        fen3 = no_avg(signal[t-2*w:t])
        [ak3, sig_sq3] = nitime.algorithms.autoregressive.AR_est_LD(
            fen3, p, rxx=None)

        carac = np.abs(2*w*np.log(np.sqrt(np.sqrt(sig_sq3))) - w *
                       np.log(np.sqrt(np.sqrt(sig_sq1))) - w*np.log(np.sqrt(np.sqrt(sig_sq2))))

        caracs.append(carac)
        t_caracs.append(time[t])
        t += 1

    return caracs, t_caracs


def sigma_samples(samples):
    N = len(samples)
    sigma = []
    mean = [samples[0]]
    mean_sq = [(samples[0])**2]
    for i in range(1, N):
        mean.append((mean[-1]*i+samples[i])/(i+1))
        mean_sq.append((mean_sq[-1]*i+samples[i]**2)/(i+1))
    sigma = []
    for i in range(N):
        sigma.append(np.sqrt(mean_sq[i]-mean[i]**2))
    return np.array(sigma), np.array(mean)



def break_times_est(feature, pos, factor, w=100):
    # calculation of threshold
    sigma,mean = sigma_samples(feature)
    threshold = factor*sigma + mean

    # detection of break
    break_times = []
    break_times_ind = []
    last_break_time = pos[0]
    for i in range(w, len(pos)):
        if feature[i] >= threshold[i]:
            if last_break_time != pos[i-1]:
                break_times.append(pos[i])
                break_times_ind.append(i)
            last_break_time = pos[i]
    return break_times, break_times_ind


def AR_break_times(numero_variable, factor, w, show=True, save=False):
    # Récupération des données et calcul de la caractéristique
    x_time, x = get_sampled_var_data.gap_sampled(numero_variable)
    carac, t_carac = AR_modelisation_nitime(x, x_time, w)
    sigma,mean = sigma_samples(carac)

    # Calcul de la rupture
    break_times = break_times_est(carac, t_carac, factor, w=w)

    if show:
        # Affichage du signal, de la caractéristique, du seuil et des ruptures détectées
        plt.figure(figsize=(12, 6))
        plt.suptitle('Ecart entre variables ' +
                     str(numero_variable)+' et '+str(numero_variable+1))

        plt.subplot(2, 1, 1)
        plt.plot(x_time, x, label="Signal")
        for t in break_times[0]:
            plt.axvline(t, color='red')
        plt.legend()

        plt.subplot(2, 1, 2)
        plt.plot(t_carac, carac, label="Caractéristique (modélisation AR)")
        plt.plot(t_carac, factor*sigma,
                 label="{} sigma".format(factor))
        plt.plot(t_carac, mean,
                 label="Moyenne".format(mean))
        plt.plot(t_carac, factor*sigma+mean,
                 label="Seuil de détection à {} sigma + moyenne".format(factor))
        for t in break_times[0]:
            plt.axvline(t, color='red')
        plt.legend()
        #plt.show()

    if save:
        nom_figure = 'images/signaux_reels_detection/variables_' + \
            str(numero_variable)+'_'+str(numero_variable+1) + \
            '_modelisation_AR.png'
        plt.savefig(nom_figure)

    return break_times