import numpy as np
import matplotlib.pyplot as plt
import get_sampled_var_data

# Ce programme cherche à détecter des ruptures de moyenne


def mean_modelisation(signal, time, w, d):
    t_means = []
    means = []
    t = w
    while t < len(signal):
        sigW = signal[t-w:t]
        sigW_mean = 0
        n = len(sigW)
        for i in range(n):
            sigW_mean += sigW[i]
        means.append(sigW_mean/n)
        t_means.append(time[t])
        t += d

    diff_means = []
    for k in range(2*w, len(signal)):
        diff_means.append(np.abs(means[k-w]-means[k-2*w]))

    return diff_means, t_means[w:len(means)]


def sigma_samples(samples):
    N = len(samples)
    sigma = []
    mean = [samples[0]]
    mean_sq = [(samples[0])**2]
    for i in range(1, N):
        mean.append((mean[-1]*i+samples[i])/(i+1))
        mean_sq.append((mean_sq[-1]*i+samples[i]**2)/(i+1))
    sigma = []
    for i in range(N):
        sigma.append(np.sqrt(mean_sq[i]-mean[i]**2))
    return np.array(sigma), np.array(mean)


def break_times_est(feature, pos, factor, w=100):
    # calculation of threshold
    sigma,mean = sigma_samples(feature)
    threshold = factor*sigma + mean

    # detection of break
    break_times = []
    break_times_ind = []
    last_break_time = pos[0]
    for i in range(w, len(pos)):
        if feature[i] >= threshold[i]:
            if last_break_time != pos[i-1]:
                break_times.append(pos[i])
                break_times_ind.append(i)
            last_break_time = pos[i]
    return break_times, break_times_ind


def mean_break_times(numero_variable, factor, w, show=True, save=False):
    # Récupération des données et calcul de la caractéristique
    x_time, x = get_sampled_var_data.gap_sampled(numero_variable)
    carac, t_carac = mean_modelisation(x, x_time, w, 1)
    sigma, mean = sigma_samples(carac)

    # Calcul de la rupture
    break_times = break_times_est(carac, t_carac, factor, w=w)

    if show:
        # Affichage du signal, de la caractéristique, du seuil et des ruptures détectées
        plt.figure(figsize=(12, 6))
        plt.suptitle('Ecart entre variables ' +
                     str(numero_variable)+' et '+str(numero_variable+1))

        plt.subplot(2, 1, 1)
        plt.plot(x_time, x, label="Signal")
        for t in break_times[0]:
            plt.axvline(t, color='red')
        plt.legend()

        plt.subplot(2, 1, 2)
        plt.plot(t_carac, carac,
                 label="Caractéristique (modélisation des moyennes)")
        plt.plot(t_carac, factor*sigma,
                 label="{} sigma".format(factor))
        plt.plot(t_carac, mean,
                 label="Moyenne".format(mean))
        plt.plot(t_carac, factor*sigma+mean,
                 label="Seuil de détection à {} sigma + moyenne".format(factor))
        for t in break_times[0]:
            plt.axvline(t, color='red')
        plt.legend()
        # plt.show()

    if save:
        nom_figure = 'images/signaux_reels_detection/variables_' + \
            str(numero_variable)+'_'+str(numero_variable+1) + \
            '_modelisation_mean.png'
        plt.savefig(nom_figure)

    return break_times
