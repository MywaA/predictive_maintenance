import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import get_data

# fig 4.12 à 4.22 notamment (génère aussi d'autres images)

# Ce programme permet de récupérer le signal sous-échantillonné d'un facteur N (en utilisant un filtre moyenneur "centré") correspondant à un écart entre une mesure et sa consigne donné (et de l'afficher / le sauvegarder / l'écrire).

def echantillonnage(x_time, x1, x2, liste_nom_variables, N=100, write=False, show=False, save=False):
    x = x1-x2
    #plt.figure()
    #plt.plot(x_time,x) # affichage du signal original
    x_ech = []
    x_ech_time = []
    # pos est en nombre d'echantillons, le temps correspondant est x_time[pos]
    pos = int(N//2)

    # calcul moyenne sur les N echantillons autour de l'instant considere, tous les N echantillons
    while pos+N//2 < len(x):
        x_ech_time.append(x_time[pos])
        m = 0
        ech = x[pos-int(N//2):pos+int(N//2)]
        for j in ech:
            m += j
        x_ech.append(int(m/len(ech)))
        pos += N

    # affichage du signal echantillonne
    plt.rcParams["figure.figsize"] = [16, 9]
    fig, ax = plt.subplots()
    ax.plot(x_ech_time, x_ech, label='Ecart entre ' +
            str(liste_nom_variables[numero_variable])+' ('+str(numero_variable)+') et '+str(liste_nom_variables[numero_variable+1])+' ('+str(numero_variable+1)+')')

    xfmt = mdates.DateFormatter('%H:%M (%d-%m-%y)')
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(8)
    ax.xaxis.set_major_formatter(xfmt)
    ax.set_xlabel('Temps')
    ax.set_ylabel(' ')
    ax.legend()

    if save:
        nom_figure = 'signaux_reels/images/variables_' + \
            str(numero_variable)+'_'+str(numero_variable+1) + '.png'
        plt.savefig(nom_figure)
    if show:
        plt.show()

    if write:
        nom_fichier = 'signaux_reels/data/variables_' + \
            str(numero_variable)+'_'+str(numero_variable+1)+'.txt'
        fichier = open(nom_fichier, "w", encoding='utf-8')
        compteur_ligne = 0
        for i in range(0, len(x_ech)):
            compteur_ligne = compteur_ligne + 1
            ligne = str(x_ech_time[i])
            ligne = ligne + "xx" + str(x_ech[i])
            ligne = ligne + "\n"
            fichier.write(ligne)

        fichier.close()
        print('Ecriture dans le fichier :', nom_fichier)

    return x_ech_time, x_ech


N = 100  # facteur d'echantillonnage
x_time, x, liste_nom_variables = get_data.read_file()
for numero_variable in range(0, 83, 2):
    print(numero_variable)
    x_ech_time, x_ech = echantillonnage(
        x_time, x[:, numero_variable], x[:, numero_variable+1], liste_nom_variables, N=N, write=False, show=True, save=False)
