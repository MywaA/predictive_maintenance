import datetime
import re
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import get_data

def ecriture_graphiques_signaux(x_time, x_mesure, x_consigne, nom_variable_mesure, nom_variable_consigne,save=False):
    # La fenêtre occupe la totalité de l'écran
    plt.rcParams["figure.figsize"] = [16, 9]
    # La fenetre contient deux graphiques qui auront la meme
    # echelle sur l'axe des ordonnees. Les deux graphiques
    # seront places l'un au dessus de l'autre dans la fenetre.
    # Sur le 1er graphique, on affiche le signal de mesure
    # et la consigne
    fig, ax = plt.subplots(2, 1, sharey=True)
    ax[0].plot(x_time, x_mesure, label=nom_variable_mesure)
    ax[0].plot(x_time, x_consigne, label=nom_variable_consigne)

    xfmt = mdates.DateFormatter('%H:%M (%d-%m-%y)')
    for tick in ax[0].xaxis.get_major_ticks():
        tick.label.set_fontsize(8)
    ax[0].xaxis.set_major_formatter(xfmt)
    ax[0].set_xlabel('time')
    ax[0].set_ylabel('degré')
    # On affiche la légende du 1er graphique
    ax[0].legend()

    x_difference = x_mesure - x_consigne
    ax[1].plot(x_time, x_difference, label='Ecart entre "' + nom_variable_mesure + '" et "' + nom_variable_consigne + '"')
    # L'axe des abscisses est l'axe temporel
    xfmt = mdates.DateFormatter('%H:%M (%d-%m-%y)')
    for tick in ax[1].xaxis.get_major_ticks():
        tick.label.set_fontsize(8)
    ax[1].xaxis.set_major_formatter(xfmt)
    ax[1].set_xlabel('time')
    ax[1].set_ylabel('degré')
    ax[1].legend()
    nom_figure = 'images/preprocessing/figure_' + nom_variable_mesure + '.png'
    if save:
        plt.savefig(nom_figure)
    plt.show()

x_time, x, liste_nom_variables = get_data.read_file()

# On enregistre dans un fichier .png un graphique qui affiche la mesure
# et la consigne,  et un graphique qui affiche la différence entre la
# mesure et la consigne.
# Variable 20 : température utile TIC0300
# Variable 21 : Consigne en cours TIC0300
numero_variable = 20
ecriture_graphiques_signaux(x_time, x[:, numero_variable], x[:, numero_variable + 1], liste_nom_variables[numero_variable],liste_nom_variables[numero_variable + 1],save=False)