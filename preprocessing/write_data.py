import datetime
import re
import numpy as np
import get_data

# --------------------------------------------------------------------------------
# Fonction qui enregistre dans un fichier texte le signal contenu dans la
# variable x. Les paramètres de la fonction sont :
#    - x_time      : une liste contenant des valeurs qui représentent le temps
#    - x           : un tableau à une dimension contenant les valeurs du signal à enregistrer dans le fichier
#    - nom_fichier : le nom du fichier dans lequel on enregistre les valeurs de x
# --------------------------------------------------------------------------------
def ecriture_fichier(x_time, x, nom_fichier):
    # On ouvre un nouveau fichier en écriture
    fichier = open(nom_fichier, "w", encoding='utf-8')
    print(fichier)

    # On enregistre les valeurs contenues dans x_time et x
    compteur_ligne = 0
    for i in range(0, len(x)):
        compteur_ligne = compteur_ligne + 1
        ligne = str(x_time[i])
        ligne = ligne + " " + str(x[i])
        ligne = ligne + "\n"
        fichier.write(ligne)

    fichier.close()
    print("Nombre de lignes dans le fichier ", compteur_ligne)

    print('Ecriture dans le fichier :', nom_fichier)
# Fin de la fonction ecriture_fichier
# --------------------------------------------------------------------------------


# --------------------------------------------------------------------------------
# Début du programme principal
# --------------------------------------------------------------------------------

# Lecture du fichier de données
x_time, x, liste_nom_variables = get_data.read_file()

# Ecriture dans un fichier des valeurs de la variable n°20
numero_variable = 20
nom_fichier = 'preprocessing/fichier_mesures_variable_' + str(numero_variable) + '.txt'
ecriture_fichier(x_time, x[:,numero_variable], nom_fichier)