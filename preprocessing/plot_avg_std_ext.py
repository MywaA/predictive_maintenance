import get_data
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

color = ['#377eb8', '#ff7f00', '#4daf4a', '#f781bf', '#a65628', '#984ea3', '#999999', '#e41a1c', '#dede00']


def floating(x, x_time, w, d):
    N = min(len(x), len(x_time))
    x_avg = []  # Moyenne
    x_std_avg = []  # Moyenne ecart type
    x_time_avg = []  # Nouvelle position des points
    for i in range(int(w/2), N-int(w/2), d):  # pas de d, car sous echantillonage
        avg = 0.0
        for j in range(i-int(w/2), i+int(w/2)):
            avg += x[j]
        x_avg.append(avg/w)
        std = 0.0
        for j in range(i-int(w/2), i+int(w/2)):
            std += (x[j]-x_avg[-1])**2
        std /= w
        x_std_avg.append(np.sqrt(std))
        x_time_avg.append(x_time[i])
    return np.array(x_avg), np.array(x_std_avg), np.array(x_time_avg)


def get_ext(x, x_time, w, d):
    N = min(len(x), len(x_time))
    x_max = []
    x_min = []
    x_time_avg = []
    for i in range(int(w/2), N-int(w/2), d):
        maxi, mini = x[i], x[i]
        for j in range(i-int(w/2), i+int(w/2)):
            if x[j] > maxi:
                maxi = x[j]
            elif x[j] < mini:
                mini = x[j]
        x_max.append(maxi)
        x_min.append(mini)
        x_time_avg.append(x_time[i])
    return np.array(x_max), np.array(x_min), np.array(x_time_avg)


def plot_floatting(x, x_time, name, setting, save=False):
    plt.rcParams["figure.figsize"] = [16, 9]
    fig, ax = plt.subplots(1, 1)

    if setting == "floating_avg":  # On affiche les moyennes flottantes
        x_avg_1, x_dev, x_time_avg_1 = floating(x, x_time, 10800, 600)
        x_avg_2, x_dev, x_time_avg_2 = floating(x, x_time, 1800, 600)
        ax.plot(x_time, x, label='Ecart à la consgine pour {}'.format(
            name), color=color[0])
        ax.plot(x_time_avg_1, x_avg_1, label="µ 3H", color=color[1])
        ax.plot(x_time_avg_2, x_avg_2, label="µ 30min", color=color[2])

    elif setting == "floating_dev":  # On affiche l'écart type flottant
        x_avg, x_dev_avg, x_time_avg = floating(x, x_time, 1800, 600)
        x_1 = x_avg-x_dev_avg
        x_2 = x_avg+x_dev_avg
        ax.plot(x_time, x, label='Ecart à la consgine pour {}'.format(
            name), color=color[0])
        ax.plot(x_time_avg, x_avg, label="µ", color=color[1])
        ax.plot(x_time_avg, x_1, label="µ-σ", color=color[2])
        ax.plot(x_time_avg, x_2, label="µ+σ", color=color[3])
        ax.fill_between(x_time_avg, x_2, x_1, where=x_2 >
                        x_1, facecolor='silver')

    elif setting == "extremum":  # Pour afficher les extrememums
        x_2, x_1, x_time_avg = get_ext(x, x_time, 1800, 600)
        x_avg, x_dev_avg, x_time_avg = floating(x, x_time, 1800, 600)
        ax.plot(x_time, x, label='Ecart à la consgine pour {}'.format(
            name), color=color[0])
        ax.plot(x_time_avg, x_avg, label="µ", color=color[1])
        ax.plot(x_time_avg, x_1, label="min", color=color[2])
        ax.plot(x_time_avg, x_2, label="max", color=color[3])
        ax.fill_between(x_time_avg, x_2, x_1, where=x_2 >
                        x_1, facecolor='silver')

    # Pour correctement afficher le temps
    xfmt = mdates.DateFormatter('%H:%M (%d-%m-%y)')
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(8)
    ax.xaxis.set_major_formatter(xfmt)
    ax.set_xlabel('Temps')
    ax.set_ylabel('Degré')
    ax.legend()

    if save:
        plt.savefig('images/preprocessing/figure_' + name + '_' + setting + '.png')
    plt.show()


numero_variable = 20

x_time, x, liste_nom_variables = get_data.read_file()

x_mesure = x[:, numero_variable]
x_consigne = x[:, numero_variable + 1]
x_mes_name = liste_nom_variables[numero_variable]
x_con_name = liste_nom_variables[numero_variable+1]

x = x_mesure - x_consigne

plot_floatting(x, x_time, x_mes_name, setting="floating_avg", save=False)  # fig 2.1
plot_floatting(x, x_time, x_mes_name, setting="floating_dev", save=False)  # fig 2.2
plot_floatting(x, x_time, x_mes_name, setting="extremum", save=False)  # fig 2.3
